<Query Kind="Statements" />

string ShowCard(int cardNumber) //Constant Pattern 
{
	return cardNumber switch
	{
		13 => "King",
		12 => "Queen",
		11 => "Jack",
		_ => "Pip card" // equivalent to 'default'
	};
}