<Query Kind="Statements" />

bool x = 2 == 9; 

switch (x)
{
	case bool b when b == true: // Fires only when b is true
		Console.WriteLine("True!");
		break;
	case bool b:
		Console.WriteLine("False!");
		break;
}
