<Query Kind="Statements" />

static string LifeStageAtAge(int age)
{
	string lifeStage = null;
	
	if (age < 0)
	{
		lifeStage = "Prenatal";
	}
	else if (age < 2)
	{
		lifeStage = "Infant";
	}
	else if (age < 4)
	{
		lifeStage = "Toddler";
	}
	else if (age < 6)
	{
		lifeStage = "EarlyChild";
	}
	else if (age < 12)
	{
		lifeStage = "MiddleChild";
	}
	else if (age < 20)
	{
		lifeStage = "Adolescent";
	}
	else if (age < 40)
	{
		lifeStage = "EarlyAdult";
	}
	else if (age < 65)
	{
		lifeStage = "MiddleAdult";
	}
	else
	{
		lifeStage = "LateAdult";
	}
	
	return lifeStage;
}

static string LifeStageAtAge1(int age)
{
	if (age < 0)
	{
		return "Prenatal";
	}
	else if (age < 2)
	{
		return "Infant";
	}
	else if (age < 4)
	{
		return "Toddler";
	}
	else if (age < 6)
	{
		return "EarlyChild";
	}
	else if (age < 12)
	{
		return "MiddleChild";
	}
	else if (age < 20)
	{
		return "Adolescent";
	}
	else if (age < 40)
	{
		return "EarlyAdult";
	}
	else if (age < 65)
	{
		return "MiddleAdult";
	}
	else
	{
		return "LateAdult";
	}
}

static string LifeStageAtAge2(int age)
{
	switch (age)
	{
		case < 0:
			return "Prenatal";
		case < 2:
			return "Infant";
		case < 4:
			return "Toddler";
		case < 6:
			return "EarlyChild";
		case < 12:
			return "MiddleChild";
		case < 20:
			return "Adolescent";
		case < 40:
			return "EarlyAdult";
		case < 65:
			return "MiddleAdult";
		default:
			return "LateAdult";
	}
}

static string LifeStageAtAge3(int age) => age switch //Relational Patterns
{
	< 0 => "Prenatal",
	< 2 => "Infant",
	< 4 => "Toddler",
	< 6 => "EarlyChild",
	< 12 => "MiddleChild",
	< 20 => "Adolescent",
	< 40 => "EarlyAdult",
	< 65 => "MiddleAdult",
	_ => "LateAdult",
};
