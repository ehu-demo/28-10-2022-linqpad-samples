<Query Kind="Statements" />

TellMeTheType(12);
TellMeTheType("hello");
TellMeTheType('a');
TellMeTheType(true);

void TellMeTheType(object x)   // object allows any type. 
{
	if (x is int i)
	{
		Console.WriteLine("It's an int!"); Console.WriteLine($"The square of {i} is {i * i}");
	}
	else if(x is string s)
	{
		Console.WriteLine("It's a string"); Console.WriteLine($"The length of {s} is {s.Length}");
	}
	else if (x is char _)
	{
		Console.WriteLine("It's a char");
	}
	else
	{
		Console.WriteLine("I don't know what x is");
	}
}

void TellMeTheType1(object x)   // object allows any type. 
{
	switch (x)
	{
		case int i:
			Console.WriteLine("It's an int!"); Console.WriteLine($"The square of {i} is {i * i}");
			break;
		case string s:
			Console.WriteLine("It's a string"); Console.WriteLine($"The length of {s} is {s.Length}");
			break;
		case char _: Console.WriteLine("It's a char"); break;
		default:
			Console.WriteLine("I don't know what x is");
			break;
	}
}
